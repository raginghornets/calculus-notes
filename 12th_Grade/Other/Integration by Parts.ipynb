{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## https://www.mathsisfun.com/calculus/integration-by-parts.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Integration by Parts\n",
    "\n",
    "Integration by Parts is a special method of integration that is often useful when two functions are multiplied together, but is also helpful in other ways.\n",
    "\n",
    "You will see plenty of examples soon, but first let us see the rule:\n",
    "\n",
    "$$\\int \\color{royalblue}{u}\\color{limegreen}{v} ~ dx = \\color{royalblue}{u} \\color{limegreen}{\\int v ~ dx} - \\int \\color{royalblue}{u'} \\left(\\color{limegreen}{\\int v ~ dx}\\right) ~ dx$$\n",
    "\n",
    "- $u$ is the function $u(x)$\n",
    "- $v$ is the function $v(x)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: What is $\\int x \\cos(x) ~ dx$ ?\n",
    "\n",
    "Ok, we have $x$ *multiplied by* $\\cos(x)$, so integration by parts is a good choice.\n",
    "\n",
    "First choose which functions for $u$ and $v$:\n",
    "\n",
    "- $\\color{royalblue}{u = x}$\n",
    "- $\\color{limegreen}{v = \\cos(x)}$\n",
    "\n",
    "### So now it is in the format $\\int uv ~ dx$\n",
    "\n",
    "Differentiate $u$: $\\color{royalblue}{u' = x' = 1}$\n",
    "\n",
    "Integrate $v$: $\\color{limegreen}{\\int v ~ dx = \\int \\cos(x) ~ dx = \\sin(x)}$\n",
    "\n",
    "Now put everything together:\n",
    "\n",
    "$$\\begin{align}\n",
    "\\int \\color{royalblue}{x} \\color{limegreen}{\\cos(x)} ~ dx &= \\color{royalblue}{x} \\color{limegreen}{\\sin(x)} - \\int \\color{royalblue}{1} \\left(\\color{limegreen}{\\sin(x)}\\right) ~ dx \\\\\n",
    "&= x \\sin(x) - \\int \\sin(x) ~ dx \\\\\n",
    "&= x \\sin(x) + \\cos(x) + C\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we followed these steps:\n",
    "\n",
    "- Choose $u$ and $v$\n",
    "\n",
    "- Differentiate $u$: $u'$\n",
    "\n",
    "- Integrate $v$: $\\int v ~ dx$\n",
    "\n",
    "- Put $u$, $u'$, and $\\int v ~ dx$ into: $u \\int v ~ dx - \\int u' \\left(\\int v ~ dx\\right) ~ dx$\n",
    "\n",
    "- Simplify and solve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In English, to help you remember, $\\int uv ~ dx$ becomes:\n",
    "\n",
    "$$\\text{(u integral v) minus integral of (derivative u, integral v)}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: What is $\\int \\ln(x) / x^2 ~ dx$ ?\n",
    "\n",
    "$$\\begin{align}\n",
    "u &= \\ln(x) \\\\\n",
    "v &= x^{-2} \\\\\n",
    "\\int \\ln(x) / x^2 ~ dx &= \\ln(x) \\int x^{-2} ~ dx - \\int \\ln(x) ~ \\frac{d}{dx} ~ \\left(\\int x^{-2} ~ dx\\right) ~ dx \\\\\n",
    "&= \\ln(x) \\left(-\\frac{1}{x}\\right) - \\int \\frac{1}{x} \\left(-\\frac{1}{x}\\right) ~ dx \\\\\n",
    "&= -\\frac{\\ln(x)}{x} - \\int -x^{-2} ~ dx \\\\\n",
    "&= -\\frac{\\ln(x)}{x} - x^{-1} + C \\\\\n",
    "&= \\frac{-(\\ln(x) + 1)}{x} + C\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: What is $\\int \\ln(x) ~ dx$ ?\n",
    "\n",
    "$$\\begin{align}\n",
    "u &= \\ln(x) \\\\\n",
    "v &= 1 \\\\\n",
    "\\int \\ln x \\cdot 1 ~ dx &= \\ln x \\cdot x - \\int \\frac{1}{x} (x) ~ dx \\\\\n",
    "&= x \\ln x - \\int 1 ~ dx \\\\\n",
    "&= x \\ln x - x + C\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: What is $\\int e^x x ~ dx$ ?\n",
    "\n",
    "$$\\begin{align}\n",
    "u &= e^x \\\\\n",
    "v &= x \\\\\n",
    "\\int e^x x ~ dx &= e^x \\int x ~ dx - \\int e^x ~ \\frac{d}{dx} ~ \\left(\\int x ~dx\\right) ~ dx \\\\\n",
    "&= e^x \\frac{x^2}{2} - e^x \\frac{x^2}{2} + C \\\\\\\\\n",
    "&\\hline\n",
    "\\text{Try again.} \\\\\n",
    "\\hline \\\\\n",
    "u &= x \\\\\n",
    "v &= e^x \\\\\n",
    "\\int e^x x ~ dx\n",
    "&= x \\int e^x ~ dx - \\int x ~ \\frac{d}{dx} ~ \\left(\\int e^x ~ dx\\right) ~ dx \\\\\n",
    "&= x e^x - \\int 1 (e^x) ~ dx \\\\\n",
    "&= x e^x - e^x + C \\\\\n",
    "&= e^x (x - 1) + C\n",
    "\\end{align}$$\n",
    "\n",
    "A helpful rule of thumb is `I LATE`. Choose $u$ based on which of these comes first:\n",
    "\n",
    "- **I**: Inverse trigonometric functions such as $\\arcsin(x)$, $\\arccos(x)$, $\\arctan(x)$\n",
    "\n",
    "- **L**: Logarithmic functions such as $\\ln(x)$, $\\log(x)$\n",
    "\n",
    "- **A**: Algebraic functions such as $x^2$, $x^3$\n",
    "\n",
    "- **T**: Trigonometric functions such as $\\sin(x)$, $\\cos(x)$, $\\tan(x)$\n",
    "\n",
    "- **E**: Exponential functions such as $e^x$, $3^x$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: $\\int e^x \\sin(x) ~ dx$\n",
    "\n",
    "$$\\begin{align}\n",
    "u &= \\sin(x) \\\\\n",
    "v &= e^x \\\\\n",
    "\\int e^x \\sin(x) ~ dx &= \\sin(x) \\int e^x ~ dx - \\int e^x ~ \\frac{d}{dx} \\left(\\int \\sin(x) ~ dx\\right) ~ dx \\\\\n",
    "&= \\sin(x) \\, e^x - \\int \\cos(x) \\left(e^x\\right) ~ dx \\\\\\\\\n",
    "\\hline\n",
    "\\text{Continue.} \\\\\n",
    "\\hline \\\\\n",
    "u &= \\cos(x) \\\\\n",
    "v &= e^x \\\\\n",
    "\\int \\cos(x) \\left(e^x\\right) ~ dx &= \\cos(x) \\int e^x ~ dx - \\int \\cos(x) \\frac{d}{dx} \\left(\\int e^x ~ dx\\right) ~ dx \\\\\n",
    "&= \\cos(x) \\, e^x - \\int -\\sin(x) \\, e^x ~ dx \\\\\n",
    "\\int e^x \\sin(x) \\, dx &= \\sin(x) \\, e^x - \\left(\\cos(x) \\, e^x - \\int -\\sin(x) \\, e^x ~ dx\\right) \\\\\n",
    "&= \\sin(x) \\, e^x - \\left(e^x \\cos(x) + \\int e^x \\sin(x) ~ dx\\right) \\\\\n",
    "&= \\sin(x) \\, e^x - e^x \\cos(x) - \\int e^x \\sin(x) ~ dx \\\\\n",
    "2 \\int e^x \\sin(x) \\, dx &= e^x \\sin(x) - e^x \\cos(x) \\\\\n",
    "\\int e^x \\sin(x) ~ dx &= \\frac{e^x \\left(\\sin(x) - \\cos(x)\\right)}{2} + C\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Footnote: Where Did \"Integration by Parts\" Come From?\n",
    "\n",
    "It is based on the Product Rule for Derivatives:\n",
    "\n",
    "$$(uv)' = uv' + u'v$$\n",
    "\n",
    "Integrate both sides and rearrange:\n",
    "\n",
    "$$\\begin{align}\n",
    "\\int (uv)' ~ dx &= \\int uv' ~ dx + \\int u'v ~ dx \\\\\n",
    "uv &= \\int uv' ~ dx + \\int u'v ~ dx \\\\\n",
    "\\int uv' ~ dx &= uv - \\int u'v ~ dx\n",
    "\\end{align}$$\n",
    "\n",
    "Some people prefer that last form, but I like to integrate $v'$ so the left side is simple:\n",
    "\n",
    "$$\\int uv ~ dx = u \\int v ~ dx - \\int u' \\left(\\int v ~ dx\\right) ~ dx$$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.3",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
