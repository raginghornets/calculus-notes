# 6.6 Double Integrals

Definition: If $f(x, y)$ is defined over the rectangular region $R$ bounded by $x = a$, $x = b$, $y = c$, and $y = d$, then the **double integral** of $f(x,y)$ over $R$ is given by $$\int_c^d \int_a^b f(x,y) dx dy \quad\text{ or }\quad \int_a^b \int_c^d f(x,y) dy dx$$

## Example 1

$$\int_3^6 \int_{-1}^2 10xy^2 dx dy$$

$$\begin{aligned}
&= \int_3^6 \left[10y^2 \int_{-1}^2 x dx\right] dy \\
&= \int_3^6 \left[10y^2 \cdot \frac{x^2}{2} \bigg|_{-1}^2\right] dy \\
&= \int_3^6 5y^2 \left(2^2 - (-1)^2\right) dy \\
&= \int_3^6 15y^2 dy \\
&= 15 \int_3^6 y^2 dy \\
&= 15 \cdot \frac{y^3}{3} \bigg|_3^6 \\
&= 5 \left(6^3 - 3^3\right) \\
&= 5 (189) \\
&= 945
\end{aligned}$$

**Note:** We may interchange our limits of integration as long as we also interchange $dy$ and $dx$. For example,

$$\int_3^6 \int_{-1}^2 10xy^2 dx dy$$

is the same as

$$\int_{-1}^2 \int_3^6 10xy^2 dy dx$$

## Example 2

$$\int_{1}^2 \int_3^6 10xy^2 dy dx$$

$$\begin{aligned}
&= \int_{-1}^2 \left[10x \int_3^6 y^2 dy\right] \\
&= \int_{-1}^2 \left[10x \cdot \frac{y^3}{3} \bigg|_3^6\right] \\
&= \int_{-1}^2 \left[10x \cdot 63\right] \\
&= \int_{-1}^2 630x dx \\
&= 630 \int_{-1}^2 x dx \\
&= 630 \cdot \frac{x^2}{2} \bigg|_{-1}^2 \\
&= 315\left(2^2 - \left(-1\right)^2\right) \\
&= 315 (3) \\
&= 945
\end{aligned}$$

## Example 3

$$\int_0^1 \int_{x^2}^x xy^2 dy dx$$

$$\begin{aligned}
&= \int_0^1 \left(x \int_{x^2}^x y^2 dy\right) dx \\
&= \int_0^1 \left(x \cdot \frac{y^3}{3} \bigg|_{x^2}^x\right) dx \\
&= \int_0^1 \frac{1}{3} x \left(x^3 - \left(x^2\right)^3\right) dx \\
&= \frac{1}{3} \int_0^1 \left(x^4 - x^7\right) dx \\
&= \frac{1}{3} \left(\frac{x^5}{5} - \frac{x^8}{8}\right) \bigg|_0^1 \\
&= \frac{1}{40}
\end{aligned}$$

