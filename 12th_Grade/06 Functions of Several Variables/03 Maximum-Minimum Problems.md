# 6.3 Maximum-Minimum Problems

Def) A function $f$ of two variables:

1. Has a relative maximum at $(a, b)$ if $f(x, y) \leq f(a, b)$ for all points in a rectangular region containing $(a, b)$;
2. Has a relative minimum at $(a, b)$ if $f(x, y) \geq f(a, b)$ for all points in a rectangular region containing $(a, b)$.

We will assume $f$ and its partial derivatives exist and are "continuous," but note we are not defining continuity.

Also, if a function achieves a relative maximum (or minimum) at $(a, b)$, and we fix $y$ as a constant, we must have $f_x(x, b) = 0$.

**Critical point** - A point at which $f_x = 0$ and $f_y = 0$.

**Saddle point** - A point $(a, b)$ at which $f_x = 0, f_y = 0$, and the function does not reach a relative maximum or minimum.

## The $D$-Test

To find the relative maximum and minimum values of $f$:

1. Find $f_x$, $f_y$, $f_{xx}$, $f_{yy}$, and $f_{xy}$.
2. Solve the system of equations $f_x = 0$, $f_y = 0$.
   Let $(a, b)$ represent a solution.
3. Evaluate $D$, where $D = f_{xx}(a, b) \cdot f_{yy}(a, b) - \left[f_{xy}(a, b)\right]^2$.
4. Then:
    - $f$ has a maximum at $(a, b)$ if $D > 0$ and $f_{xx}(a, b) < 0$.
    - $f$ has a minimum at $(a, b)$ if $D > 0$ and $f_{xx}(a, b) > 0$.
    - $f$ has neither a maximum nor a minimum at $(a, b)$ if $D < 0$. The function has a *saddle point* at $(a, b)$.
    - This test is not applicable if $D = 0$.


## Example 1

Find the relative maximum or minimum values of $f(x, y) = x^2 + xy + y^2 - 3x$.

### Solution

#### Step 1

$$\begin{aligned}
f_x &= 2x + y - 3 \\
f_{xx} &= 2 \\
f_y &= x + 2y \\
f_{yy} &= 2 \\
f_{xy} &= 1
\end{aligned}$$

#### Step 2

$$\begin{aligned}
2x + y &= 3 \\
2(x + 2y) &= 0 \\
\hline
-3y &= 3 \\
y &= -1 \\
x &= 2
\end{aligned}$$

#### Step 3

$D = 2 \cdot 2 - (1)^2 = 4 - 1 = 3$

#### Step 4

$f_{xx} (2, -1) = 2 > 0$

$(2, -1)$ is a relative minimum

$f(2, -1) = -3$, so there is a minimum value at $(2, -1, -3)$.

## Example 2

$f(x, y) = xy - x^3 - y^2$

### Solution

#### Step 1

$$\begin{aligned}
f_x &= y - 3x^2 \\
f_{xx} &= -6x \\
f_y &= x - 2y \\
f_{yy} &= -2 \\
f_{xy} &= 1
\end{aligned}$$

#### Step 2

$$\begin{aligned}
y - 3x^2 &= 0 \\
x - 2y &= 0 \\
\hline
x - 2(3x^2) &= 0 \\
x (1 - 6x) &= 0 \\
x = 0 &\text{ or } 1 - 6x = 0 \\
x = 0 &\text{ or } x = \frac{1}{6} \\
\hline
x = 2y &\quad x = 2y \\
0 = 2y &\quad \frac{1}{6} = 2y \\
0 = y &\quad \frac{1}{12} = y \\
(0, 0) &\quad \left(\frac{1}{6}, \frac{1}{12}\right)
\end{aligned}$$

#### Step 3

$D(0, 0) = -1$, since $D < 0$, saddle point

$D\left(\frac{1}{6}, \frac{1}{12}\right) = 1$, since $D > 0$ and $f_{xx} < 0$

#### Step 4

$f\left(\frac{1}{6}, \frac{1}{12}\right) = \frac{1}{6} \cdot \frac{1}{12} - \left(\frac{1}{6}\right)^3 - \left(\frac{1}{12}\right)^2 = \frac{1}{72} - \frac{1}{216} - \frac{1}{144} = \frac{1}{432}$, so there is a relative maximum at $\left(\frac{1}{6}, \frac{1}{12}, \frac{1}{432}\right)$

## Example 3

Revenue: $R(x, y) = 3x + 2y$

Cost: $C(x, y) = 2x^2 - 2xy + y^2 - 9x + 6y + 7$

Profit: $P(x, y) = R(x, y) - C(x, y) = -2x^2 + 2xy + y^2 + 12x - 4y - 7$
